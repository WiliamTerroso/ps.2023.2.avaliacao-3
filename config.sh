#!/bin/bash
source func.sh

# Configurações disponíveis
echo "Escolha a customização do PS1:"
echo "1. Mudar a cor"
echo "2. Exibir apenas o nome do usuário"
echo "3. Prompt em duas linhas"

read -p "Digite o número da opção desejada: " choice

case $choice in
    1)
        read -p "Digite o código da cor desejada (ex: 31 para vermelho): " color_code
        set_color $color_code
        ;;
    2)
        set_username
        ;;
    3)
        set_two_lines
        ;;
    *)
        echo "Opção inválida."
        ;;
esac

