#!/bin/bash

# Função para aplicar a cor ao PS1
set_color() {
    PS1="\[\e[$1m\]$2\[\e[0m\] "
}

# Função para exibir apenas o nome do usuário no PS1
set_username() {
    PS1="\u "
}

# Função para configurar o PS1 em duas linhas
set_two_lines() {
    PS1='\[\e[0;36m\]\u@\h \[\e[0m\]\n\$ '
}

